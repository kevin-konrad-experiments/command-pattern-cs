﻿using System;
using TPCommand.Commands.BankAccounts;
using TPCommand.Receivers;

namespace TPCommand.Screens {
    public class AccountCreateScreen {
        private readonly User _user;

        public AccountCreateScreen(User user) {
            _user = user;
        }

        public void Show() {
            Console.Clear();
            Console.WriteLine("========== NEW BANK ACCOUNT ==========");

            if (_user.BankAccounts == null || _user.BankAccounts.Count == 0) {
                Console.WriteLine("You don't have any bank account yet, please create one first.");
            } else {
                _user.DisplayBankAccounts();
            }


            int id = new Random().Next(0, 999999);
            Console.WriteLine($"Id: {id}");
            Console.WriteLine($"Owner: {_user.Firstname} {_user.Lastname}");
            decimal amount = ScreenUtils.GetDecimalInput("Select the starting amount you want to deposit (euros): ");
            BankAccount bankAccount = new BankAccount(id, _user, amount);

            BankAccountCreate bankAccountCreateCommand = new BankAccountCreate(bankAccount, _user);
            CommandsManager commandsManager = CommandsManager.GetInstance();
            commandsManager.Do(bankAccountCreateCommand);
        }
    }
}
