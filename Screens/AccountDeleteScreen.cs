﻿using System;
using TPCommand.Commands.BankAccounts;
using TPCommand.Receivers;

namespace TPCommand.Screens {
    public class AccountDeleteScreen {
        private readonly User _user;

        public AccountDeleteScreen(User user) {
            _user = user;
        }

        public void Show() {
            Console.Clear();
            Console.WriteLine("========== DELETE BANK ACCOUNT ==========");

            if (_user.BankAccounts == null || _user.BankAccounts.Count == 0) {
                Console.WriteLine("You don't have any bank account yet, please create one first.");
                return;
            }

            _user.DisplayBankAccounts();

            BankAccount bankAccount = ScreenUtils.GetBankAccountInput(_user, "Enter the ID of the bank account you want to delete: ");

            BankAccountDelete bankAccountDeleteCommand = new BankAccountDelete(bankAccount, _user);
            CommandsManager commandsManager = CommandsManager.GetInstance();
            commandsManager.Do(bankAccountDeleteCommand);
        }
    }
}
