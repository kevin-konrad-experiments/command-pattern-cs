﻿using System;
using TPCommand.Receivers;

namespace TPCommand.Screens {
    class UserMenuScreen : IScreen {
        private readonly User _user;
        private readonly CommandsManager _commandsManager;

        public UserMenuScreen(User user) {
            _commandsManager = CommandsManager.GetInstance();
            _user = user;
        }

        public void Show() {
            Console.Clear();
            Console.WriteLine("========== USER PANEL ==========");
            Console.WriteLine($"Hello {_user.Firstname} {_user.Lastname}, here is an overview of your accounts:");

            if (_user.BankAccounts == null || _user.BankAccounts.Count == 0) {
                Console.WriteLine("You don't have any bank account yet, please create one by typing 1.");
            }

            _user.DisplayBankAccounts();

            DisplayMenu();
        }

        private void DisplayMenu() {
            Console.WriteLine("MENU");
            Console.WriteLine("1. Create a bank account");
            Console.WriteLine("2. Delete a bank account");
            Console.WriteLine("3. Deposit money on a bank account");
            Console.WriteLine("4. Transfer money from a bank account to another");
            Console.WriteLine("5. Withdraw money from a bank account");
            Console.WriteLine("6. Update your user information");
            Console.WriteLine("7. Undo last command");
            Console.WriteLine($"8. Undo all commands of this session ({_commandsManager.GetCommandsCount()} commands)");
            Console.WriteLine("9. Redo last command");
            Console.WriteLine("10. Log out");
            Console.WriteLine();

            GetMenuChoice();
        }

        private void GetMenuChoice() {
            int choice = ScreenUtils.GetIntegerInput("What do you want to do? (enter the corresponding number): ");

            switch (choice) {
                case 1:
                    AccountCreate();
                    break;
                case 2:
                    AccountDelete();
                    break;
                case 3:
                    AccountDeposit();
                    break;
                case 4:
                    AccountTransfer();
                    break;
                case 5:
                    AccountWithdraw();
                    break;
                case 6:
                    UpdateUser();
                    break;
                case 7:
                    UndoLast();
                    break;
                case 8:
                    UndoAll();
                    break;
                case 9:
                    RedoLast();
                    break;
                case 10:
                    return;
            }

            Show();
        }

        private void AccountCreate() {
            AccountCreateScreen screen = new AccountCreateScreen(_user);
            screen.Show();

            Console.Write("\nPress any key to return to user panel...");
            Console.ReadKey();
        }

        private void AccountDelete() {
            AccountDeleteScreen screen = new AccountDeleteScreen(_user);
            screen.Show();

            Console.Write("\nPress any key to return to user panel...");
            Console.ReadKey();
        }

        private void AccountDeposit() {
            AccountDepositScreen screen = new AccountDepositScreen(_user);
            screen.Show();

            Console.Write("\nPress any key to return to user panel...");
            Console.ReadKey();
        }

        private void AccountTransfer() {
            AccountTransferScreen screen = new AccountTransferScreen(_user);
            screen.Show();

            Console.Write("\nPress any key to return to user panel...");
            Console.ReadKey();
        }

        private void AccountWithdraw() {
            AccountWithdrawScreen screen = new AccountWithdrawScreen(_user);
            screen.Show();

            Console.Write("\nPress any key to return to user panel...");
            Console.ReadKey();
        }

        private void UpdateUser() {
            UserUpdateScreen screen = new UserUpdateScreen(_user);
            screen.Show();

            Console.Write("\nPress any key to return to user panel...");
            Console.ReadKey();
        }

        private void UndoLast() {
            _commandsManager.UndoLast();

            Console.Write("\nPress any key to return to user panel...");
            Console.ReadKey();
        }

        private void UndoAll() {
            _commandsManager.UndoAll();

            Console.Write("\nPress any key to return to user panel...");
            Console.ReadKey();
        }

        private void RedoLast() {
            _commandsManager.RedoLast();

            Console.Write("\nPress any key to return to user panel...");
            Console.ReadKey();
        }
    }
}
