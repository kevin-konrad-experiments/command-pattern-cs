﻿using System;
using TPCommand.Commands.BankAccounts;
using TPCommand.Receivers;

namespace TPCommand.Screens {
    public class AccountWithdrawScreen {
        private readonly User _user;

        public AccountWithdrawScreen(User user) {
            _user = user;
        }

        public void Show() {
            Console.Clear();
            Console.WriteLine("========== BANK WITHDRAW ACCOUNT ==========");

            if (_user.BankAccounts == null || _user.BankAccounts.Count == 0) {
                Console.WriteLine("You don't have any bank account yet, please create one first.");
                return;
            }

            _user.DisplayBankAccounts();

            BankAccount bankAccount = ScreenUtils.GetBankAccountInput(_user, "Enter the ID of the bank account you want to withdraw from: ");
            decimal amount = ScreenUtils.GetDecimalInput("Select the amount you want to withdraw (euros): ");

            BankAccountWithdraw bankAccountWithdrawCommand = new BankAccountWithdraw(bankAccount, amount);
            CommandsManager commandsManager = CommandsManager.GetInstance();
            commandsManager.Do(bankAccountWithdrawCommand);
        }
    }
}
