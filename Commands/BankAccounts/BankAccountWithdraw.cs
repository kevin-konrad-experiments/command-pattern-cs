﻿using System;
using TPCommand.Receivers;

namespace TPCommand.Commands.BankAccounts {
    public class BankAccountWithdraw : ICommand {
        private readonly BankContext _context;
        private readonly BankAccount _bankAccount;
        private readonly decimal _amount;

        public BankAccountWithdraw(BankAccount bankAccount, decimal amount) {
            _context = BankContext.GetInstance();
            _bankAccount = bankAccount;
            _amount = amount;
        }

        public bool Execute() {
            if (_bankAccount.Balance < _amount) {
                Console.WriteLine($"[Account {_bankAccount.Id}] Insufficient founds. Current balance: {_bankAccount.Balance} euros");
                return false;
            }

            if (_amount < 5) {
                Console.WriteLine("Minimum withdraw amount is 5.00 euros.");
                return false;
            }

            _bankAccount.Balance -= _amount;
            _context.SaveChanges();
            Console.WriteLine($"[Account {_bankAccount.Id}] Withdrawal of {_amount} euros successful. Current balance: {_bankAccount.Balance} euros");
            return true;
        }

        public bool Undo() {
            _bankAccount.Balance += _amount;
            _context.SaveChanges();
            Console.WriteLine($"[Account {_bankAccount.Id}] Withdrawal of {_amount} euros undone. Current balance: {_bankAccount.Balance} euros");
            return true;
        }
    }
}
