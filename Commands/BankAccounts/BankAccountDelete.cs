﻿using System;
using TPCommand.Receivers;

namespace TPCommand.Commands.BankAccounts {
    public class BankAccountDelete : ICommand {
        private readonly BankContext _context;
        private readonly BankAccount _bankAccount;
        private readonly User _user;

        public BankAccountDelete(BankAccount bankAccount, User user) {
            _context = BankContext.GetInstance();
            _bankAccount = bankAccount;
            _user = user;
        }

        public bool Execute() {
            if (_bankAccount == null || _user == null) {
                Console.WriteLine("Invalid bank account or user values");
                return false;
            }

            if (_bankAccount.Balance > 0) {
                Console.WriteLine($"Bank account cannot be deleted because not empty. Current balance: {_bankAccount.Balance} euros");
                return false;
            }

            _user.BankAccounts.Remove(_bankAccount);
            _context.SaveChanges();
            Console.WriteLine($"Account {_bankAccount.Id} removed from user {_user.Firstname} {_user.Lastname}.");
            return true;
        }

        public bool Undo() {
            if (_bankAccount == null || _user == null) {
                Console.WriteLine("Invalid bank account or user values");
                return false;
            }

            _user.BankAccounts.Add(_bankAccount);
            _context.SaveChanges();
            Console.WriteLine($"Account deletion undone.");
            return true;
        }
    }
}
