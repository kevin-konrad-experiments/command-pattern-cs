﻿using System;
using TPCommand.Receivers;

namespace TPCommand.Commands.BankAccounts {
    public class BankAccountDeposit : ICommand {
        private readonly BankContext _context;
        private readonly BankAccount _bankAccount;
        private readonly decimal _amount;

        public BankAccountDeposit(BankAccount bankAccount, decimal amount) {
            _context = BankContext.GetInstance();
            _bankAccount = bankAccount;
            _amount = amount;
        }

        public bool Execute() {
            if (_amount < 10) {
                Console.WriteLine("Minimum deposit amount is 10.00 euros.");
                return false;
            }

            _bankAccount.Balance += _amount;
            _context.SaveChanges();
            Console.WriteLine($"[Account {_bankAccount.Id}] Deposit of {_amount} euros successful. Current balance: {_bankAccount.Balance} euros");
            return true;
        }

        public bool Undo() {
            if (_bankAccount.Balance < _amount) {
                Console.WriteLine($"[Account {_bankAccount.Id}] Insufficient founds. Current balance: {_bankAccount.Balance} euros");
                return false;
            }

            _bankAccount.Balance -= _amount;
            _context.SaveChanges();
            Console.WriteLine($"[Account {_bankAccount.Id}] Deposit of {_amount} euros undone. Current balance: {_bankAccount.Balance} euros");
            return true;
        }
    }
}
