﻿using System;
using TPCommand.Receivers;

namespace TPCommand.Commands.BankAccounts {
    public class BankAccountTransfer : ICommand {
        private readonly BankContext _context;
        private readonly BankAccount _fromAccount;
        private readonly BankAccount _toAccount;
        private readonly decimal _amount;

        public BankAccountTransfer(BankAccount fromAccount, BankAccount toAccount, decimal amount) {
            _context = BankContext.GetInstance();
            _fromAccount = fromAccount;
            _toAccount = toAccount;
            _amount = amount;
        }

        public bool Execute() {
            if (_fromAccount.Balance < _amount) {
                Console.WriteLine($"[Account {_fromAccount.Id}] Insufficient founds. Current balance: {_fromAccount.Balance} euros");
                return false;
            }

            _fromAccount.Balance -= _amount;
            _toAccount.Balance += _amount;
            _context.SaveChanges();

            Console.WriteLine($"Transfer of {_amount} euros from account {_fromAccount.Id} ({_fromAccount.Owner.Firstname} {_fromAccount.Owner.Lastname}) to account {_toAccount.Id} ({_toAccount.Owner.Firstname} {_toAccount.Owner.Lastname}) successfull.");
            Console.WriteLine($"[Account {_fromAccount.Id} ({_fromAccount.Owner.Firstname} {_fromAccount.Owner.Lastname})] Current balance: {_fromAccount.Balance} euros");
            Console.WriteLine($"[Account {_toAccount.Id} ({_toAccount.Owner.Firstname} {_toAccount.Owner.Lastname})] Current balance: {_toAccount.Balance} euros");
            return true;
        }

        public bool Undo() {
            if (_toAccount.Balance < _amount) {
                Console.WriteLine($"[Account {_toAccount.Id}] Insufficient founds. Current balance: {_toAccount.Balance} euros");
                return false;
            }

            _fromAccount.Balance += _amount;
            _toAccount.Balance -= _amount;
            _context.SaveChanges();

            Console.WriteLine($"Transfer of {_amount} euros from account {_fromAccount.Id} ({_fromAccount.Owner.Firstname} {_fromAccount.Owner.Lastname}) to account {_toAccount.Id} ({_toAccount.Owner.Firstname} {_toAccount.Owner.Lastname}) undone.");
            Console.WriteLine($"[Account {_fromAccount.Id} ({_fromAccount.Owner.Firstname} {_fromAccount.Owner.Lastname})] Current balance: {_fromAccount.Balance} euros");
            Console.WriteLine($"[Account {_toAccount.Id} ({_toAccount.Owner.Firstname} {_toAccount.Owner.Lastname})] Current balance: {_toAccount.Balance} euros");
            return true;
        }
    }
}
